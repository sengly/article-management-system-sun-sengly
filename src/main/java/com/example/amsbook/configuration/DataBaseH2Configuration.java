package com.example.amsbook.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.example.amsbook.repository")
public class DataBaseH2Configuration {

    @Bean
    @Profile("development")
    public DataSource development(){
        EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
        embeddedDatabaseBuilder.setType(EmbeddedDatabaseType.H2);
        embeddedDatabaseBuilder.addScript("sql/table.sql");
        embeddedDatabaseBuilder.addScript("sql/insert.sql");
        embeddedDatabaseBuilder.addScript("sql/tableCategory.sql");
        embeddedDatabaseBuilder.addScript("sql/dataCategory.sql");
        return embeddedDatabaseBuilder.build();
    }

}