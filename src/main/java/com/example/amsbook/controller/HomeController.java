package com.example.amsbook.controller;

import com.example.amsbook.model.Book;
import com.example.amsbook.model.Category;
import com.example.amsbook.service.book.BookService;
import com.example.amsbook.service.category.CategoryService;
import com.example.amsbook.utility.Filter;
import com.example.amsbook.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.naming.IdentityNamingStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Controller
public class HomeController {

    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @Autowired
    private CategoryService categoryService;


    @GetMapping("/")
    public String home(Model model, Paging paging, Filter filter){
        paging.setTotalRecord(bookService.count());
        paging.setRecordToShow(3);
        paging.setPage(1);
        paging.setTotalPage(paging.getTotalRecord(),paging.getRecordToShow());
        paging.setOffset(paging.getRecordToShow(),paging.getTotalRecord());
        paging.setPageToShow(5);
        model.addAttribute("paging",paging);
        model.addAttribute("filter", new Filter());
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("books",bookService.findAll(paging.getRecordToShow(),paging.getOffset(),filter));
        System.out.println(filter.getId());
        return "book/index";
    }

    @GetMapping("/category")
    public String category(Model model){
        model.addAttribute("categories",categoryService.findAll());
        return "category/category";
    }

    @GetMapping("/addCategory")
    public String addCategory(Model model){
        model.addAttribute("category",new Category());
        model.addAttribute("isAdd",true);
        return "category/add";
    }

    @PostMapping("/addCategory")
    public String insertCategory(@ModelAttribute Category category){
        categoryService.add(category);
        return "redirect:/category";
    }

    @GetMapping("/updateCategory/{id}")
    public String update(Model model,@PathVariable Integer id){
        System.out.println("Id :"+id);
        model.addAttribute("category",categoryService.findById(id));
        model.addAttribute("isAdd",false);
        return "category/add";
    }

    @PostMapping("/updateCategory")
    public String updateCategory(@ModelAttribute Category category){
        categoryService.updateCategory(category);
        return "redirect:/category";
    }


    @GetMapping("/add")
    public String add(ModelMap modelMap){
        modelMap.addAttribute("categories",categoryService.findAll());
        modelMap.addAttribute("book", new Book());
        modelMap.addAttribute("isAdd",true);
        return "book/add";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable Integer id,Model model){
        model.addAttribute("book",bookService.findById(id));
        return "book/view";
    }

    @GetMapping("/search/{filterId}")
    public String search(@RequestParam("s") String s,@RequestParam("id") Integer filterID,Filter filter, Model model)
    {
//        paging.setTotalRecord(bookService.countSearch(s));
//        paging.setRecordToShow(3);
//        paging.setPage(page);
//        paging.setTotalPage(paging.getTotalRecord(),paging.getRecordToShow());
//        paging.setOffset(paging.getRecordToShow(),paging.getTotalRecord());
//        model.addAttribute("paging",paging);
        filter.setId(filterID);
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("books",bookService.search(s,filter));
        return "book/search";
    }

    @GetMapping("/page/{page}")
    public String page(Model model,@PathVariable("page") Integer page,Paging paging,Filter filter){
        paging.setTotalRecord(bookService.count());
        paging.setRecordToShow(3);
        paging.setPage(page);
        paging.setTotalPage(paging.getTotalRecord(),paging.getRecordToShow());
        paging.setOffset(paging.getRecordToShow(),paging.getTotalRecord());
        model.addAttribute("paging",paging);
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("books",bookService.findAll(paging.getRecordToShow(),paging.getOffset(),filter));
        return "book/index";
    }

    @PostMapping("/add")
    public String insertBook(@RequestParam("file")MultipartFile file, @Valid @ModelAttribute Book book, BindingResult result, Model model){
        String fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
        book.setImage(fileName);
        model.addAttribute("categories",categoryService.findAll());

        if (result.hasErrors()){
            model.addAttribute("book", book);
            model.addAttribute("isAdd",true);
            return "book/add";
        }
        if (!file.isEmpty())
        {
            System.out.println(file.getOriginalFilename());
            try {
                Files.copy(file.getInputStream(), Paths.get("src/main/resources/img",fileName));
                book.setImage(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        bookService.add(book);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String deleteBook(@PathVariable Integer id){
        bookService.removeById(id);
        return "redirect:/";
    }

    @GetMapping("/deleteCategory/{id}")
    public String deleteCategory(@PathVariable Integer id){
        categoryService.delete(id);
        return "redirect:/category";
    }

    @GetMapping("/update/{id}")
    public String update(ModelMap modelMap,@PathVariable Integer id){
        modelMap.addAttribute("book",bookService.findById(id));
        modelMap.addAttribute("isAdd",false);
        return "book/add";
    }

    @PostMapping("/update")
    public String updateBook(ModelMap modelMap,@ModelAttribute Book book,@RequestParam("file")MultipartFile file,BindingResult result){
        String fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
        if (result.hasErrors()){
            modelMap.addAttribute("book", book);
            modelMap.addAttribute("isAdd",true);
            return "book/add";
        }
        if (!file.isEmpty())
        {
            System.out.println(file.getOriginalFilename());
            try {
                Files.copy(file.getInputStream(), Paths.get("src/main/resources/img",fileName));
                book.setImage(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else{
            book.setImage(bookService.findById(book.getId()).getImage());
        }
        bookService.update(book);
        return "redirect:/";
    }




}
