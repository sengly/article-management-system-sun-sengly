package com.example.amsbook.service.book;

import com.example.amsbook.model.Book;
import com.example.amsbook.model.Category;
import com.example.amsbook.repository.book.BookRepository;
import com.example.amsbook.service.book.BookService;
import com.example.amsbook.utility.Filter;
import com.example.amsbook.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class BookServiceImp implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> findAll(int recordToShow, int offset, Filter filter)
    {
        return bookRepository.findAll(recordToShow,offset,filter);
    }

    @Override
    public Integer countSearch(String s) {
        return bookRepository.countSearch(s);
    }

    @Override
    public Book findById(int id) {
        return bookRepository.findById(id);
    }

    @Override
    public void add(Book book) {
        book.setDate(new Date().toString());
        bookRepository.add(book);
    }

    @Override
    public List<Book> search(String s,Filter filter) {
        return bookRepository.search(s,filter);
    }

    @Override
    public void removeById(int id) {
        bookRepository.removeById(id);
    }


    @Override
    public void update(Book book) {
        book.setDate(new Date().toString());
        bookRepository.update(book);
    }

    @Override
    public Integer count() {
        return bookRepository.count();
    }
}
