package com.example.amsbook.service.book;

import com.example.amsbook.model.Book;
import com.example.amsbook.utility.Filter;
import com.example.amsbook.utility.Paging;

import java.util.List;

public interface BookService {
    List<Book> findAll(int recordToShow, int offset, Filter filter);
    Book findById(int id);
    void add(Book book);
    void removeById(int id);
    void update(Book book);
    List<Book> search(String s,Filter filter);
    Integer count();
    Integer countSearch(String s);
}
