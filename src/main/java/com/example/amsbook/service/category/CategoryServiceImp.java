package com.example.amsbook.service.category;

import com.example.amsbook.model.Category;
import com.example.amsbook.repository.category.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void add(Category category) {
        categoryRepository.add(category);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(int id) {
        return categoryRepository.findById(id);
    }

    @Override
    public void updateCategory(Category category) {
        categoryRepository.updateCategory(category);
        System.out.println("update:"+category);
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }
}
