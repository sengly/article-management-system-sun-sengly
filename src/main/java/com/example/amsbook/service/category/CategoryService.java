package com.example.amsbook.service.category;

import com.example.amsbook.model.Category;
import com.example.amsbook.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {


    List<Category> findAll();

    Category findById(int id);

    void add(Category category);

    void updateCategory(Category category);

    void delete(int id);

}
