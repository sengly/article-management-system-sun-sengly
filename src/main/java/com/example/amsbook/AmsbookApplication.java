package com.example.amsbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmsbookApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmsbookApplication.class, args);
    }

}
