//package com.example.amsbook.repository.category;
//
//import com.example.amsbook.model.Category;
//import org.springframework.stereotype.Repository;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository
//public class CategoryRepositoryImp implements CategoryRepository{
//
//    List<Category> categories = new ArrayList<>();
//
//    public CategoryRepositoryImp() {
//        categories.add(new Category(1,"Spring"));
//        categories.add(new Category(2,"Web"));
//        categories.add(new Category(3,"Java"));
//        categories.add(new Category(4,"Database"));
//        categories.add(new Category(5,"Korean"));
//    }
//
//    @Override
//    public List<Category> findAll() {
//        return categories;
//    }
//
//    @Override
//    public Category findById(int id) {
//        for (int i = 0; i < categories.size(); i++) {
//            if (categories.get(i).getId()==id){
//                return categories.get(i);
//            }
//        }
//        return null;
//    }
//}
