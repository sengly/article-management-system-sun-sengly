package com.example.amsbook.repository.category;

import com.example.amsbook.model.Category;
import com.example.amsbook.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @SelectProvider(method = "findAll",type = CategoryProvider.class)
    @Results({
            @Result(property = "id",column = "category_id"),
            @Result(property = "name",column = "category_name")
    })
    List<Category> findAll();

    @SelectProvider(method = "findById",type = CategoryProvider.class)
    @Results({
            @Result(property = "id",column = "category_id"),
            @Result(property = "name",column = "category_name")
    })
    Category findById(int id);

    @SelectProvider(method = "insert",type = CategoryProvider.class)
    void add(Category category);

    @SelectProvider(method = "updateCategory",type = CategoryProvider.class)
    void updateCategory(Category category);

    @SelectProvider(method = "delete",type = CategoryProvider.class)
    void delete(int id);

}
