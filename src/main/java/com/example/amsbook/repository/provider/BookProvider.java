package com.example.amsbook.repository.provider;

import com.example.amsbook.model.Book;
import com.example.amsbook.model.Category;
import com.example.amsbook.utility.Filter;
import com.example.amsbook.utility.Paging;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String findAll(int recordToShow, int offset, Filter filter){
        if (filter.getId()!=0){
            return "select b.id,b.title,b.author,b.description,b.create_date,b.image," +
                    "c.category_name,b.category_id " +
                    "from tb_book b inner join tb_category c " +
                    "on b.category_id = c.category_id limit "
                    +recordToShow+" offset "+offset+" where b.category_id = "+filter.getId() ;
        }
        return "select b.id,b.title,b.author,b.description,b.create_date,b.image," +
                "c.category_name,b.category_id " +
                "from tb_book b inner join tb_category c " +
                "on b.category_id = c.category_id limit "
                +recordToShow+" offset "+offset ;

    }
    public String findById(Integer id){
        return new SQL(){{
            SELECT("id,title,description,author,image,create_date");
            FROM("tb_book");
            WHERE("id ="+id);
        }}.toString();
    }

    public String removeById(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_book");
            WHERE("id="+id);
        }}.toString();
    }
    public String update(Integer id){
        return new SQL(){{
            UPDATE("tb_book");

        }}.toString();
    }

    public String insert(Book book){
        return new SQL(){{
            INSERT_INTO("tb_book");
            VALUES("title","'"+book.getTitle()+"'");
            VALUES("author","'"+book.getAuthor()+"'");
            VALUES("description","'"+book.getDescription()+"'");
            VALUES("image","'"+book.getImage()+"'");
            VALUES("create_date","'"+book.getDate()+"'");
            VALUES("category_id","'"+ book.getCategory().getId()+"'");
        }}.toString();
    }

    public String updateBook(Book book){
        return new SQL(){{
            UPDATE("tb_book");
            SET("title=#{title}");
            SET("author=#{author}");
            SET("description=#{description}");
            SET("image=#{image}");
            SET("create_date=#{date}");
            WHERE("id="+book.getId());
        }}.toString();
    }

    public String count(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_book");
        }}.toString();

    }

    public String countSearch(String s){
        return "SELECT COUNT(id) FROM TB_BOOK b " +
                "INNER JOIN TB_CATEGORY c ON b.category_id = c.category_id " +
                "where b.title ILIKE '%"+s+"%'";
    }

    public String search(String s,Filter filter){
        if (filter.getId()!=0){
            return "SELECT b.id,b.title,b.author,b.description,b.create_date,b.image,c.category_name," +
                    "b.category_id " +
                    "FROM TB_BOOK b " +
                    "INNER JOIN TB_CATEGORY c ON b.category_id = c.category_id " +
                    "where b.title ILIKE '%"+s+"%' and b.category_id = "+filter.getId() ;

        }
        return "select b.id,b.title,b.author,b.description,b.create_date,b.image," +
                "c.category_name,b.category_id " +
                "from tb_book b inner join tb_category c " +
                "on b.category_id = c.category_id "
                +"where b.title ilike '%"+s+"%'" ;
    }




}
