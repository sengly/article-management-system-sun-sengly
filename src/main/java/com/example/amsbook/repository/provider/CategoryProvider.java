package com.example.amsbook.repository.provider;

import com.example.amsbook.model.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String findAll(){
        return new SQL(){{
            SELECT("category_id,category_name");
            FROM("tb_category");
            ORDER_BY("category_id");
        }} .toString();
    }
    public String findById(Integer id){
        return new SQL(){{
            SELECT("category_id,category_name");
            FROM("tb_category");
            WHERE("category_id="+id);
        }} .toString();
    }

    public String insert(Category category){
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("category_name","'"+category.getName()+"'");
        }}.toString();
    }

    public String updateCategory(Category category){
        return new SQL(){{
            UPDATE("tb_category");
            SET("category_name=#{name}");
            WHERE("category_id="+category.getId());
        }}.toString();
    }
    public String delete(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("category_id="+id);
        }}.toString();
    }
}
