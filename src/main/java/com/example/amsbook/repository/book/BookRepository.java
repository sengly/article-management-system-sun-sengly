package com.example.amsbook.repository.book;

import com.example.amsbook.model.Book;
import com.example.amsbook.repository.provider.BookProvider;
import com.example.amsbook.utility.Filter;
import com.example.amsbook.utility.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {


//    @Select("SELECT b.id,b.title,b.author,b.description,b.create_date,b.image,c.category_name,b.category_id FROM TB_BOOK b " +
//            "INNER JOIN TB_CATEGORY c ON b.category_id = c.category_id")
    @SelectProvider(method = "findAll",type = BookProvider.class)
    @Results({
            @Result(property ="date",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "category_name")
    })
    List<Book> findAll(int recordToShow, int offset, Filter filter);


    @Select("SELECT b.id,b.title,b.author,b.description,b.create_date,b.image,c.category_name,b.category_id FROM TB_BOOK b " +
            "INNER JOIN TB_CATEGORY c ON b.category_id = c.category_id where b.id=#{id}")
    @Results({
            @Result(property ="date",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "category_name")
    })
    Book findById(int id);

    @SelectProvider(method = "insert",type = BookProvider.class)
    void add(Book book);

    @SelectProvider(method = "removeById",type = BookProvider.class)
    void removeById(int id);

    @SelectProvider(method = "updateBook",type = BookProvider.class)
    void update(Book book);

    @SelectProvider(method = "search",type = BookProvider.class)
    @Results({
            @Result(property ="date",column = "create_date"),
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "category_name")
    })
    List<Book> search(String s,Filter filter);

    @SelectProvider(method = "count",type = BookProvider.class)
    Integer count();

    @SelectProvider(method = "countSearch",type = BookProvider.class)
    Integer countSearch(String s);
}
