//package com.example.amsbook.repository;
//
//import com.example.amsbook.model.Book;
//import com.github.javafaker.Faker;
//import org.springframework.stereotype.Repository;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//@Repository
//public class BookRepositoryImp implements BookRepository{
//    List<Book> books = new ArrayList<>();
//    String fileName ="1cf28dd2-fb4d-4392-b4e3-0852ba54e815depositphotos_166763326-stock-illustration-vector-illustration-of-bearded-coder.jpg";
//    public BookRepositoryImp(){
//        Faker f = new Faker();
//        for (int i=1;i<=10;i++){
//            books.add(new Book(i,f.book().title(),f.book().genre(),f.book().author(),new Date().toString(),fileName));
//        }
//    }
//
//
//    @Override
//    public List<Book> findAll() {
//        return books;
//    }
//
//    @Override
//    public Book findById(int id) {
//        for (int i = 0; i < books.size(); i++) {
//            if (books.get(i).getId()==id){
//                return books.get(i);
//            }
//        }
//        return null;
//    }
//
//    @Override
//    public void add(Book book) {
//        books.add(book);
//    }
//
//    @Override
//    public void removeById(int id) {
//        for (int i = 0; i < books.size(); i++) {
//            if (books.get(i).getId()==id){
//                books.remove(books.get(i));
//            }
//        }
//    }
//
//    @Override
//    public void update(Book book) {
//        for (int i = 0; i < books.size(); i++) {
//            if(books.get(i).getId()==book.getId()){
//                books.set(i,book);
//            }
//        }
//    }
//}
