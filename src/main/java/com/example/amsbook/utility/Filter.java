package com.example.amsbook.utility;

public class Filter {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "id=" + id +
                '}';
    }
}
