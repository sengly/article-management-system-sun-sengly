package com.example.amsbook.utility;


public class Paging {

    private int totalRecord;
    private int recordToShow;
    private int totalPage;
    private int offset;
    private int page;
    private int pageToShow;
    private int startPage;
    private int endPage;

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getEndPage() {
        return endPage;
    }

    public void setEndPage(int endPage) {
        this.endPage = endPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getPageToShow() {
        return pageToShow;
    }

    public void setPageToShow(int pageToShow) {
        this.pageToShow = pageToShow;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getRecordToShow() {
        return recordToShow;
    }

    public void setRecordToShow(int recordToShow) {
        this.recordToShow = recordToShow;
    }

    public void setTotalPage(int totalRecord,int recordToShow) {
        this.totalPage = (int) Math.ceil((double)totalRecord/(double) recordToShow);
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int recordToShow,int totalRecord) {
        this.offset = (page*recordToShow)-recordToShow;
    }

    public int getPage() {
        return page;
    }

    @Override
    public String toString() {
        return "Paging{" +
                " totalRecord=" + totalRecord +
                ", recordToShow=" + recordToShow +
                ", totalPage=" + totalPage +
                ", offset=" + offset+
                ", page=" + page +
                '}';
    }

    public void setPage(int page) {
        this.page = page;
    }


}
